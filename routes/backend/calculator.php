<?php 
    use App\Http\Controllers\Backend\CalculatorController; 

    Route::get('quote-calculator',[
        'uses'=>'CalculatorController@index',
        'as'=>'quote-cal'
    ]);

    Route::get('autocomplete/fetch',[
        'uses'=>'CalculatorController@fetch',
        'as'=>'autocomplete.fetch'
    ]);

    Route::get('getplacename',[
        'uses'=>'CalculatorController@getPlaceNameByPostalCode',
        'as'=>'getplacename'
    ]);

    Route::get('getbaserate',[
        'uses'=>'CalculatorController@getBaseRate',
        'as'=>'getbaserate'
    ]);
?>