<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use DB;

class CalculatorController extends Controller
{
    public function index(){
        return view('backend.quote-calculator.index');
    }

    public function fetch(Request $request){
        if($request->get('query')){
            $query = $request->get('query');

            $data = DB::table('postal_codes')
            ->where('postal_code', 'LIKE', "%{$query}%")
            ->get();

            $output = '<ul class="dropdown-menu" style="display:block; position:relative;width:44%;">';

            foreach($data as $row){
                $output .= '<li id="'.$row->place_name.'"><a href="#" class="suggested_codes">'.$row->postal_code.'</a></li>';
            }

            $output .= '</ul>';
            echo $output;
        }
    }

    public function getPlaceNameByPostalCode(Request $request){
        $postal_code=$request->postal_code; 
        $data="";
        if($postal_code!="" && $postal_code!=null){ 
            $data=DB::table('postal_codes')->where('postal_code','=',$postal_code)->first(); 
        }       

        return Response::json(array('place'=>$data->place_name));
    }

    public function getBaseRate(Request $request){
        $postal_code=$request->postal_code;
        $data="";

        if($postal_code!="" && $postal_code!=null){ 
            $data=DB::table('postal_codes')->where('postal_code','=',$postal_code)->first(); 
        }       

        return Response::json(array('base_rate'=>$data->base_rate));
    }
}
