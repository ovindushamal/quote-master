@extends('backend.layouts.app')

@section('css')
    <style type="text/css">
        .postal-code-div, .postal-code-div .form-group, .postal-code-div .form-group label{
            padding-left: 0;
        }

        #postal-code-lists {
            padding: 0;
        }

        .suggested_codes{
            padding-left: 4%;
        }

        #box-content{
            margin-left: 2%;
        }

        .box_qty{
            text-align: center;
        }

        #title{
            text-align: center;
            border: #c8ced3 solid 1px;
        }

        #title h5{
            margin: 0;
            padding: 1% 0;
        }

        #calculations-section {
            margin-top: 2%;            
            padding: 0.6% 0;
        }

        #title-des {
            margin-top: 2%;
        }

        #estimated-box-count{            
            display: inline-block;
        }

        #estimated-box-count-no{            
            color: #ffffff;
            background-color:red;
            display: inline-block;
            padding: 1% 5%;
        }        

        #zone h6{
            display: inline-block;
        }

        .bold-text{
            font-weight: bold;
        }

        .site{
            text-align: center;
        }

        .site-name{
            border: #c8ced3 solid 1px;
        }

        #site-type {
            margin-top: 2%;
        }

        .blue-row{
            background-color: #0070C0;
            color:white;
        }

        .blue-row td, .white-row td{
            font-weight: bold;
        }

        .green-row{
            background-color: #66FF33;
        }

        .white-row{
            color: red;            
        }
        
    </style>
@endsection

@section('content')    

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">                       
                        Quote-Calculator
                    </h4>
                </div>
            </div>           
            <hr>
            <div class="row">
                <div class="postal-code-div col-md-12">
                    <div class="form-group form-inline col-md-12">
                        <label for="postal-code" class="col-sm-3">First 3 Digits of Postal Code</label>
                        <input type="text" name="postal_code" id="postal-code" class="form-control col-sm-4">
                        
                        <div id="postal-code-lists" class="col-sm-9 offset-sm-3"></div>
                    </div>
                    {{ csrf_field() }}
                </div>
            </div>
            <div class="row">
                <div id="box-content" class="col-md-6 offset-md-1">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                                <th>Box Qty</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row"># of Boxes</th>
                                <td><input type="number" name="no_of_boxes" id="no-of-boxes" min="0" class="form-control only-numeric" value="0"></td>
                                <td id="box-qty-no-of-boxes" class="box_qty">0</td>
                            </tr>
                            <tr>
                                <th scope="row"># of Grabage Bags</th>
                                <td><input type="number" name="no_of_garbage_bags" id="no-of-garbage-bags" min="0" class="form-control only-numeric" value="0"></td>
                                <td id="box-qty-no-of-garbage-bags" class="box_qty">0</td>
                            </tr>
                            <tr>
                                <th scope="row"># of Bins</th>
                                <td><input type="number" name="no_of_bins" id="no-of-bins" min="0" class="form-control only-numeric" value="0"></td>
                                <td id="box-qty-no-of-bins" class="box_qty">0</td>
                            </tr>
                            <tr>
                                <td colspan="3"></td>
                            </tr>
                            <tr>
                                <th scope="row" colspan="2">Estimated Box Count</th>
                                <td class="box_qty" id="total-box-count">0</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div id="calculations-section" class="col-md-12">
                    <div id="title" class="col-md-12">
                        <h5>Quote Calculator</h5>
                    </div>
                    <div id="title-des" class="title_des col-md-12">
                        <div class="row">
                            <div id="box-count" class="col-md-6">
                                <h6 id="estimated-box-count" class="bold-text">Estimated Box Count</h6>
                                <h6 id="estimated-box-count-no" class="bold-text">0</h6>                            
                            </div>
                            <div id="zone" class="col-md-6">
                                <h6 class="bold-text">Zone: </h6>
                                <h6 id="zone-name"></h6>
                            </div>
                        </div>                        
                    </div>
                    <div id="site-type" class="col-md-12">
                        <div class="row">
                            <div id="off-site" class="col-md-4 offset-md-2 site">
                                <h2 class="site-name col-md-12">OFF SITE</h2>
                            </div>
                            <div id="on-site" class="col-md-4 offset-md-2 site">
                                <h2 class="site-name col-md-12">ON SITE</h2>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="price-section" class="col-md-12">
                    <div class="row">
                        <div id="off-site-price" class="col-md-6">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Two Weeks</th>
                                        <th>Next Week</th>
                                        <th>This Week</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="blue-row">
                                        <th scope="row">Retail Price</th>
                                        <td>$ <span id="two-weeks-retail-off">0.00</span></td>
                                        <td>$ <span id="next-week-retail-off">0.00</span></td>
                                        <td>$ <span id="this-week-retail-off">0.00</span></td>
                                    </tr>
                                    <tr class="green-row">
                                        <th scope="row">Recommended Price</th>
                                        <td>$ <span id="two-weeks-recommended-off">0.00</span></td>
                                        <td>$ <span id="next-week-recommended-off">0.00</span></td>
                                        <td>$ <span id="this-week-recommended-off">0.00</span></td>
                                    </tr>
                                    <tr class="white-row">
                                        <th scope="row">Low Ball Price</th>
                                        <td>$ <span id="two-weeks-low-off">0.00</span></td>
                                        <td>$ <span id="next-week-low-off">0.00</span></td>
                                        <td>$ <span id="this-week-low-off">0.00</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="on-site-price" class="col-md-6">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Two Weeks</th>
                                        <th>Next Week</th>
                                        <th>This Week</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="blue-row">
                                        <th scope="row">Retail Price</th>
                                        <td>$ <span id="two-weeks-retail-on">0.00</span></td>
                                        <td>$ <span id="next-week-retail-on">0.00</span></td>
                                        <td>$ <span id="this-week-retail-on">0.00</span></td>
                                    </tr>
                                    <tr class="green-row">
                                        <th scope="row">Recommended Price</th>
                                        <td>$ <span id="two-weeks-recommended-on">0.00</span></td>
                                        <td>$ <span id="next-week-recommended-on">0.00</span></td>
                                        <td>$ <span id="this-week-recommended-on">0.00</span></td>
                                    </tr>
                                    <tr class="white-row">
                                        <th scope="row">Low Ball Price</th>
                                        <td>$ <span id="two-weeks-low-on">0.00</span></td>
                                        <td>$ <span id="next-week-low-on">0.00</span></td>
                                        <td>$ <span id="this-week-low-on">0.00</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>

        <div class="card-footer">
            <div class="row">
                <div class="col-md-1 offset-md-11">
                    <a class="btn btn-primary" href="{{url('admin/dashboard')}}">Cancel</a>
                </div>                
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script>
        $(document).ready(function(){

            var typingTimer; //timer identifier
            var doneTypingInterval = 500;  //time in ms
            var doneTypingIntervalForPostalCode=3000;

            $('#postal-code').keyup(function(){  
                clearTimeout(typingTimer);
                typingTimer = setTimeout(doneTypingPostalCode, doneTypingIntervalForPostalCode);

                var query = $(this).val();
                if(query != ''){
                    var _token = $('input[name="_token"]').val();
                    $.ajax({
                        url:"{{ url('admin/autocomplete/fetch') }}",
                        method:"GET",
                        data:{
                            query:query,
                            _token:_token
                            },
                        success:function(data){
                            $('#postal-code-lists').fadeIn();  
                            $('#postal-code-lists').html(data);
                        }
                    });
                }else{
                    $('#postal-code-lists').fadeOut();  
                }
            });

            $(document).on('click', '.dropdown-menu li', function(){  
                $('#postal-code').val($(this).text());
                $("#zone-name").html($(this).attr('id'));  
                $('#postal-code-lists').fadeOut(); 

                //get base rate for the typed postal_code

                $.ajax({
                    url: "{{url('admin/getbaserate')}}",
                    mathod: 'GET',
                    data: {
                        postal_code: $('#postal-code').val()
                    },
                    success: function(data){
                        var box_count=parseInt($("#estimated-box-count-no").text());
                        var base_rate=data.base_rate; 
                        var two_weeks_retail_off=base_rate+((box_count-10)*5);
                        $("#two-weeks-retail-off").html(Number.parseFloat(two_weeks_retail_off).toFixed(2));

                        //set value for next-week-retail-offsite
                        var next_week_retail_off=two_weeks_retail_off*1.15;
                        $("#next-week-retail-off").html(Number.parseFloat(next_week_retail_off).toFixed(2));

                        //set value for this-week-retail-offsite
                        var this_week_retail_off=two_weeks_retail_off*1.3;
                        $("#this-week-retail-off").html(Number.parseFloat(this_week_retail_off).toFixed(2));

                        //set value for two-weeks-recommended-offsite
                        var two_weeks_recommended_off=two_weeks_retail_off*0.9;
                        $("#two-weeks-recommended-off").html(Number.parseFloat(two_weeks_recommended_off).toFixed(2));

                        //set value for next-week-recommended-offsite
                        var next_week_recommended_off=next_week_retail_off*0.9;
                        $("#next-week-recommended-off").html(Number.parseFloat(next_week_recommended_off).toFixed(2));

                        //set value for this-week-recommended-offsite
                        var this_week_recommended_off=this_week_retail_off*0.97;
                        $("#this-week-recommended-off").html(Number.parseFloat(this_week_recommended_off).toFixed(2));   

                        //set value for two-weeks-retail-onsite
                        var two_weeks_retail_on=two_weeks_retail_off*1.25;
                        $("#two-weeks-retail-on").html(Number.parseFloat(two_weeks_retail_on).toFixed(2)); 

                        //set value for next-week-retail-onsite
                        var next_week_retail_on=next_week_retail_off*1.5;
                        $("#next-week-retail-on").html(Number.parseFloat(next_week_retail_on).toFixed(2));

                        // set value for this-week-retail-onsite
                        var this_week_retail_on=this_week_retail_off*1.75;
                        $("#this-week-retail-on").html(Number.parseFloat(this_week_retail_on).toFixed(2));

                        //set value for two-weeks-recommended-onsite
                        var two_weeks_recommended_on=two_weeks_retail_on*0.9;
                        $("#two-weeks-recommended-on").html(Number.parseFloat(two_weeks_recommended_on).toFixed(2));

                        //set value for next-week-recommended-onsite
                        var next_week_recommended_on=next_week_retail_on*0.9;
                        $("#next-week-recommended-on").html(Number.parseFloat(next_week_recommended_on).toFixed(2));

                        //set value for this-week-recommended-onsite
                        var this_week_recommended_on=this_week_retail_on*0.97;
                        $("#this-week-recommended-on").html(Number.parseFloat(this_week_recommended_on).toFixed(2));

                    },
                    error: function(data){
                        console.log(data);
                    }
                });                 

            });             

            /* Get the Place Name after user typed the postal code */              

            $("#postal-code").on('keydown',function(){
                clearTimeout(typingTimer);
            }); 

            function doneTypingPostalCode(){ 
                $('#postal-code-lists').fadeOut();
                var postal_code=$("#postal-code").val();  

                //get place name
                $.ajax({
                    url: "{{ url('admin/getplacename') }}",
                    method: 'GET',
                    data: {
                        postal_code: postal_code
                    },
                    success: function(data){
                        $("#zone-name").html(data.place);
                    },
                    error:function(data){
                        console.log(data);
                    }
                });

                //get base rate for the typed postal_code

                $.ajax({
                    url: "{{url('admin/getbaserate')}}",
                    mathod: 'GET',
                    data: {
                        postal_code: postal_code
                    },
                    success: function(data){
                        var box_count=parseInt($("#estimated-box-count-no").text());
                        var base_rate=data.base_rate; 
                        var two_weeks_retail_off=base_rate+((box_count-10)*5);
                        $("#two-weeks-retail-off").html(Number.parseFloat(two_weeks_retail_off).toFixed(2));

                        //set value for next-week-retail-offsite
                        var next_week_retail_off=two_weeks_retail_off*1.15;
                        $("#next-week-retail-off").html(Number.parseFloat(next_week_retail_off).toFixed(2));

                        //set value for this-week-retail-offsite
                        var this_week_retail_off=two_weeks_retail_off*1.3;
                        $("#this-week-retail-off").html(Number.parseFloat(this_week_retail_off).toFixed(2));

                        //set value for two-weeks-recommended-offsite
                        var two_weeks_recommended_off=two_weeks_retail_off*0.9;
                        $("#two-weeks-recommended-off").html(Number.parseFloat(two_weeks_recommended_off).toFixed(2));

                        //set value for next-week-recommended-offsite
                        var next_week_recommended_off=next_week_retail_off*0.9;
                        $("#next-week-recommended-off").html(Number.parseFloat(next_week_recommended_off).toFixed(2));

                        //set value for this-week-recommended-offsite
                        var this_week_recommended_off=this_week_retail_off*0.97;
                        $("#this-week-recommended-off").html(Number.parseFloat(this_week_recommended_off).toFixed(2));

                        //set value for two-weeks-retail-onsite
                        var two_weeks_retail_on=two_weeks_retail_off*1.25;
                        $("#two-weeks-retail-on").html(Number.parseFloat(two_weeks_retail_on).toFixed(2)); 

                        //set value for next-week-retail-onsite
                        var next_week_retail_on=next_week_retail_off*1.5;
                        $("#next-week-retail-on").html(Number.parseFloat(next_week_retail_on).toFixed(2));

                        // set value for this-week-retail-onsite
                        var this_week_retail_on=this_week_retail_off*1.75;
                        $("#this-week-retail-on").html(Number.parseFloat(this_week_retail_on).toFixed(2));

                        //set value for two-weeks-recommended-onsite
                        var two_weeks_recommended_on=two_weeks_retail_on*0.9;
                        $("#two-weeks-recommended-on").html(Number.parseFloat(two_weeks_recommended_on).toFixed(2));

                        //set value for next-week-recommended-onsite
                        var next_week_recommended_on=next_week_retail_on*0.9;
                        $("#next-week-recommended-on").html(Number.parseFloat(next_week_recommended_on).toFixed(2));

                        //set value for this-week-recommended-onsite
                        var this_week_recommended_on=this_week_retail_on*0.97;
                        $("#this-week-recommended-on").html(Number.parseFloat(this_week_recommended_on).toFixed(2));
                    },
                    error: function(data){
                        console.log(data);
                    }
                });
            }

            /* End Get the Place Name after user typed the postal code */       

            /* Change the No of Boxes */            

            //on keyup, start the countdown
            $("#no-of-boxes").on('keyup',function(){
                clearTimeout(typingTimer);
                typingTimer = setTimeout(doneTypingNoOfBoxes, doneTypingInterval);
            }); 

            //on keydown, clear the countdown 
            $("#no-of-boxes").on('keydown', function () {
                clearTimeout(typingTimer);
            });

            function doneTypingNoOfBoxes(){
                $("#box-qty-no-of-boxes").html($("#no-of-boxes").val());   

                setTotalNoOfBoxes();             
            }

            /* End Change the No of Boxes */

            /* Change the No of Garbage Boxes */

            $("#no-of-garbage-bags").on('keyup',function(){
                clearTimeout(typingTimer);
                typingTimer = setTimeout(doneTypingNoOfGarbageBoxes, doneTypingInterval);
            });

            $("#no-of-garbage-bags").on('keydown',function(){
                clearTimeout(typingTimer);
            });

            function doneTypingNoOfGarbageBoxes(){
                var no_of_garbage_bags=$("#no-of-garbage-bags").val();
                var final_value=Math.round(no_of_garbage_bags*2.5);
                $("#box-qty-no-of-garbage-bags").html(final_value);

                setTotalNoOfBoxes();
            }

            /* End Change the No of Garbage Boxes */

            /* Change the No of Bins */

            $("#no-of-bins").on('keyup',function(){
                clearTimeout(typingTimer);
                typingTimer = setTimeout(doneTypingNoBins, doneTypingInterval);
            });

            $("#no-of-bins").on('keydown',function(){
                clearTimeout(typingTimer);
            });

            function doneTypingNoBins(){
                var final_value=$("#no-of-bins").val()*10;
                $("#box-qty-no-of-bins").html(final_value);

                setTotalNoOfBoxes();
            }

            /* End Change the No of Bins */

            //set total no of boxes

            function setTotalNoOfBoxes(){ 
                var total_boxes=0;
                var garbage_boxes=0;
                var bins=0;

                if($("#box-qty-no-of-boxes").text()!=""){
                    total_boxes=parseInt($("#box-qty-no-of-boxes").text());
                }

                if($("#box-qty-no-of-garbage-bags").text()!=""){
                    var garbage_boxes=parseInt($("#box-qty-no-of-garbage-bags").text());
                }  

                if($("#box-qty-no-of-bins").text()!=""){
                    var bins=parseInt($("#box-qty-no-of-bins").text());
                }               

                var total=total_boxes+garbage_boxes+bins; 

                $("#total-box-count").html(total);
                $("#estimated-box-count-no").html(total);

                //get base rate for the typed postal_code

                if($("#postal-code").val()!="" && $("#postal-code").val()!=null){
                    $.ajax({
                        url: "{{url('admin/getbaserate')}}",
                        mathod: 'GET',
                        data: {
                            postal_code: $("#postal-code").val()
                        },
                        success: function(data){
                            var box_count=parseInt($("#estimated-box-count-no").text());
                            var base_rate=data.base_rate; 
                            var two_weeks_retail_off=base_rate+((box_count-10)*5);
                            $("#two-weeks-retail-off").html(Number.parseFloat(two_weeks_retail_off).toFixed(2));

                            //set value for next-week-retail-offsite
                            var next_week_retail_off=two_weeks_retail_off*1.15;
                            $("#next-week-retail-off").html(Number.parseFloat(next_week_retail_off).toFixed(2));

                            //set value for this-week-retail-offsite
                            var this_week_retail_off=two_weeks_retail_off*1.3;
                            $("#this-week-retail-off").html(Number.parseFloat(this_week_retail_off).toFixed(2));

                            //set value for two-weeks-recommended-offsite
                            var two_weeks_recommended_off=two_weeks_retail_off*0.9;
                            $("#two-weeks-recommended-off").html(Number.parseFloat(two_weeks_recommended_off).toFixed(2));

                            //set value for next-week-recommended-offsite
                            var next_week_recommended_off=next_week_retail_off*0.9;
                            $("#next-week-recommended-off").html(Number.parseFloat(next_week_recommended_off).toFixed(2));

                            //set value for this-week-recommended-offsite
                            var this_week_recommended_off=this_week_retail_off*0.97;
                            $("#this-week-recommended-off").html(Number.parseFloat(this_week_recommended_off).toFixed(2));

                            //set value for two-weeks-low-offsite
                            var two_weeks_low_off=0;
                            if(total<10){
                                two_weeks_low_off=85;                                
                            }else{
                                two_weeks_low_off=two_weeks_retail_off*0.65;
                            }

                            $("#two-weeks-low-off").html(Number.parseFloat(two_weeks_low_off).toFixed(2));

                            // set value for next-week-low-offsite
                            var next_week_low_off=0;

                            if(total<10){
                                next_week_low_off=105;
                            }else{
                                next_week_low_off=next_week_retail_off*0.7;
                            }

                            $("#next-week-low-off").html(Number.parseFloat(next_week_low_off).toFixed(2));

                            // set value for this-week-low-offsite
                            var this_week_low_off=0;
                            if(total<10){
                                this_week_low_off=115;
                            }else{
                                this_week_low_off=this_week_retail_off*0.75;
                            }

                            $("#this-week-low-off").html(Number.parseFloat(this_week_low_off).toFixed(2));

                            //set value for two-weeks-retail-onsite
                            var two_weeks_retail_on=two_weeks_retail_off*1.25;
                            $("#two-weeks-retail-on").html(Number.parseFloat(two_weeks_retail_on).toFixed(2)); 

                            //set value for next-week-retail-onsite
                            var next_week_retail_on=next_week_retail_off*1.5;
                            $("#next-week-retail-on").html(Number.parseFloat(next_week_retail_on).toFixed(2));

                            // set value for this-week-retail-onsite
                            var this_week_retail_on=this_week_retail_off*1.75;
                            $("#this-week-retail-on").html(Number.parseFloat(this_week_retail_on).toFixed(2));

                            //set value for two-weeks-recommended-onsite
                            var two_weeks_recommended_on=two_weeks_retail_on*0.9;
                            $("#two-weeks-recommended-on").html(Number.parseFloat(two_weeks_recommended_on).toFixed(2));

                            //set value for next-week-recommended-onsite
                            var next_week_recommended_on=next_week_retail_on*0.9; 
                            $("#next-week-recommended-on").html(Number.parseFloat(next_week_recommended_on).toFixed(2));

                            //set value for this-week-recommended-onsite
                            var this_week_recommended_on=this_week_retail_on*0.97;
                            $("#this-week-recommended-on").html(Number.parseFloat(this_week_recommended_on).toFixed(2));

                            // set value for two-weeks-low-onsite
                            var two_weeks_low_on=0;
                            if(total<10){
                                two_weeks_low_on=125;
                            }else{
                                two_weeks_low_on=two_weeks_retail_on*0.75;
                            }

                            $("#two-weeks-low-on").html(Number.parseFloat(two_weeks_low_on).toFixed(2));

                            //set value for next-week-low-onsite
                            var next_week_low_on=0;
                            if(total<10){
                                next_week_low_on=150;
                            }else{
                                next_week_low_on=next_week_retail_on*0.8;
                            }

                            $("#next-week-low-on").html(Number.parseFloat(next_week_low_on).toFixed(2));

                            //set value for this-week-low-onsite
                            var this_week_low_on=0;

                            if(total<10){
                                this_week_low_on=175;
                            }else{
                                this_week_low_on=this_week_retail_on*0.85;
                            }

                            $("#this-week-low-on").html(Number.parseFloat(this_week_low_on).toFixed(2));
                        },
                        error: function(data){
                            console.log(data);
                        }
                    });
                }                
            }

            // End set total no of boxes

            /* Type Only Numeric */
            $(".only-numeric").bind("keypress", function (e) {
                var keyCode = e.which ? e.which : e.keyCode;                    

                if (!(keyCode >= 48 && keyCode <= 57)) {
                    return false;
                }

            });
        });
    </script>
@endsection